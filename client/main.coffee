import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.jade';

Template.hello.onCreated ()->
  # counter starts at 0
  @counter = new ReactiveVar(0)


Template.hello.helpers(
  counter: ()->
    return Template.instance().counter.get();
)

Template.hello.events(
  'click button' :(event, instance) ->
   # // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1)

);
